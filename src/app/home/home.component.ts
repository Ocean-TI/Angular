import { UserService } from './../services/user.service';
import { User } from './../interface/user';
import { Router, NavigationStart } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public user: User;
  constructor(
    public router: Router,
    public USER: UserService) {
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe((event: NavigationStart) => {
        // You only receive NavigationStart events
        this.user = this.USER.getUser();
      });
  }
  ngOnInit() {
    this.user = this.USER.getUser();
  }
}
