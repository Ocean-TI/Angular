import { CreateBusinessComponent } from './create-business/create-business.component';
import { EditBusinessComponent } from './edit-business/edit-business.component';
import { ComponentsService } from './../../services/components.service';
import { UserService } from './../../services/user.service';
import { AppComponent } from './../../app.component';
import { User } from './../../interface/user';
import { Business } from './../../interface/business';
import { Router, NavigationStart } from '@angular/router';
import { Component, OnInit, trigger, state, style, animate, transition } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-home-client',
  templateUrl: './home-client.component.html',
  styleUrls: ['./home-client.component.scss'],
  animations: [
    trigger('shrinkOut', [
      state('in', style({ opacity: 1, transform: 'translateX(0)' })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100%)'
        }),
        animate('0.2s ease-in')
      ]),
      transition('* => void', [
        animate('0.2s 0.1s ease-out', style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ]),
    trigger('scaleEfect', [
      state('in', style({ transform: 'scale(1)' })),
      transition('void => *', [
        style({
          transform: 'scale(0)'
        }),
        animate('0.6s ease-in')
      ]),
      transition('* => void', [
        animate('0.6s ease-out', style({
          transform: 'scale(1)'
        }))
      ])
    ])
  ]
})
export class HomeClientComponent implements OnInit {
  business: any = <Business>{};
  public user: User;
  public stateBusiness: string;

  constructor(public dialog: MatDialog,
    public USER: UserService,
    public AppComponent: AppComponent,
    public COMPONENT: ComponentsService,
    public router: Router) {

    this.business = [];
    this.stateBusiness = "todos";
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe((event: NavigationStart) => {
        // You only receive NavigationStart events
        this.user = this.USER.getUser();
      });
  }

  ngOnInit() {
    this.user = this.USER.getUser();
    this.getBusinessUser(this.stateBusiness);
  }

  getBusinessUser(state) {
    let data = {
      state: state
    }
    this.USER.getBusiness(this.user.token, data).subscribe((res: any) => {
      this.business = res.response;
    }, err => {
      this.COMPONENT.openSnackBar("Se ha presentado un error al consultar la lista de negocios", "Aceptar");
    })
  }

  changeStateBusiness(data, state) {
    this.AppComponent.loading = true;
    data.state = state;
    this.USER.changeStateBusiness(this.user.token, data).subscribe((res: any) => {
      this.COMPONENT.openSnackBar(res.message, "Aceptar");
      this.AppComponent.loading = false;
      this.getBusinessUser(this.stateBusiness);
    }, err => {
      this.AppComponent.loading = false;
      this.COMPONENT.openSnackBar(err.message, "Aceptar");
    })
  }


  deleteBusiness(item) {
    this.COMPONENT.openConfirmAlert(`Eliminar el negocio ${item.name}?`, "Esta acción eliminar completamente el negocio.").then(res => {
      if (res == true) {
        this.AppComponent.loading = true;
        this.USER.deleteBusiness(this.user.token, item).subscribe((res: any) => {
          this.COMPONENT.openSnackBar(res.message, "Aceptar");
          this.AppComponent.loading = false;
          this.getBusinessUser(this.stateBusiness);
        }, err => {
          this.AppComponent.loading = false;
          this.COMPONENT.openSnackBar(err.message, "Aceptar");
        })
      }
    })
  }

  createBusiness(): void {
    this.AppComponent.loading = true;
    let dialogRef = this.dialog.open(CreateBusinessComponent, {
      width: '400px',
      data: this.user
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result == true) {
        this.AppComponent.loading = false;
        this.getBusinessUser(this.stateBusiness);
      }
      else {
        this.AppComponent.loading = false;
      }
    });
  }

  openDialogEditBusiness(item): void {
    this.AppComponent.loading = true;
    let dialogRef = this.dialog.open(EditBusinessComponent, {
      width: '360px',
      data: {
        user: this.user,
        business: item
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result == true) {
        this.AppComponent.loading = false;
        this.getBusinessUser(this.stateBusiness);
      }
      else {
        this.AppComponent.loading = false;
      }
    });
  }



}
