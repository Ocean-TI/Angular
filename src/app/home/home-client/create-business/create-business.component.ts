import { ComponentsService } from './../../../services/components.service';
import { AppComponent } from './../../../app.component';
import { UserService } from './../../../services/user.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-business',
  templateUrl: './create-business.component.html',
  styleUrls: ['./create-business.component.scss'],
  providers: [UserService, AppComponent, ComponentsService]
})
export class CreateBusinessComponent implements OnInit {
  myform: FormGroup;
  public category: any;

  constructor(
    private fb: FormBuilder,
    public USER: UserService,
    public COMPONENT: ComponentsService,
    public AppComponent: AppComponent,
    public dialogRef: MatDialogRef<CreateBusinessComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  ngOnInit() {
    this.setForm();
    this.getListCategories();
  }
  setForm() {
    this.myform = this.fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(20)])],
      'address': [null, Validators.compose([Validators.required])],
      'phone': [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(20)])],
      'category': [null, Validators.required],
      'fullname_contact': [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(20)])],

    });
  }

  createBusiness(data) {
    console.log(data.value);
    this.AppComponent.loading = true;
    this.USER.createBusiness(this.data.token, data.value).subscribe((res: any) => {
      this.AppComponent.loading = false;
      this.COMPONENT.openSnackBar(res.message, "Aceptar");
      this.dialogRef.close(true);
    }, err => {
      this.COMPONENT.openSnackBar(err.message, "Aceptar");
      this.AppComponent.loading = false;
      this.dialogRef.close();
    })
  }

  getListCategories() {
    this.USER.getElement(this.data.token, 'categories/getCategories').subscribe((res: any) => {
      this.category = res.response;
    }, err => {
      this.COMPONENT.openSnackBar("Se ha presentado un error al consultar la lista de categorias", "Aceptar");
    })
  }
}
