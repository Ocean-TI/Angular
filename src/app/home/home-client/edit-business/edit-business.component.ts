import { User } from './../../../interface/user';
import { ComponentsService } from './../../../services/components.service';
import { AppComponent } from './../../../app.component';
import { UserService } from './../../../services/user.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-business',
  templateUrl: './edit-business.component.html',
  styleUrls: ['./edit-business.component.scss'],
  providers: [UserService, AppComponent, ComponentsService]

})
export class EditBusinessComponent implements OnInit {
  myform: FormGroup;
  user: User;
  business: any;
  public category: any;
  

  constructor(
    private fb: FormBuilder,
    public USER: UserService,
    public COMPONENT: ComponentsService,
    public AppComponent: AppComponent,
    public dialogRef: MatDialogRef<EditBusinessComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.business = data.business;
    this.user = data.user;
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  ngOnInit() {
    this.getListCategories();
    this.setForm();
  }
  setForm() {
    this.myform = this.fb.group({
      'name': [this.data.business.name, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(50)])],
      'category': [this.data.business.categoryid, Validators.required],
      'address': [this.data.business.address, Validators.compose([Validators.required])],
      'phone': [this.data.business.phone, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(20)])],
      'fullname_contact': [this.data.business.fullname_contact, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(50)])],

    });
  }

  getListCategories() {
    this.USER.getElement(this.user.token, 'categories/getCategories').subscribe((res: any) => {
      this.category = res.response;
    }, err => {
      this.COMPONENT.openSnackBar("Se ha presentado un error al consultar la lista de categorias", "Aceptar");
    })
  }

  updateBusiness(data) {
    data.value.idbusiness = this.business.idbusiness;
    this.AppComponent.loading = true;
    this.USER.updateBusiness(this.user.token, data.value).subscribe((res: any) => {
      this.AppComponent.loading = false;
      this.COMPONENT.openSnackBar(res.message, "Aceptar");
      this.dialogRef.close(true);
    }, err => {
      this.COMPONENT.openSnackBar(err.message, "Aceptar");
      this.AppComponent.loading = false;
      this.dialogRef.close();
    })
  }
}
