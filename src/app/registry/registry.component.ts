import { AppComponent } from './../app.component';
import { UserService } from './../services/user.service';
import { ComponentsService } from './../services/components.service';

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-registry',
  templateUrl: './registry.component.html',
  styleUrls: ['./registry.component.scss'],
  providers: [UserService, ComponentsService]
})
export class RegistryComponent implements OnInit {
  myform: FormGroup;

  constructor(public USER: UserService,
    private router: Router,
    public AppComponent: AppComponent,
    public COMPONENT: ComponentsService,
    private fb: FormBuilder) {
  }
  ngOnInit() {
    this.myform = this.fb.group({
      'name_user': [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30)])],
      'name': [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30)])],
      'last_name': [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30)])],
      'email': [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(40)])],
      'phone': [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(20)])],
      'gender': [null, Validators.required],
      'pwd': [null, Validators.compose([Validators.required, Validators.minLength(7), Validators.maxLength(20)])],
    });
  }

  // Función para crear un usuario tipo cliente.
  createUser(data) {
    this.AppComponent.loading = true;
    // indico el rol del usuario cliente.
    data.value.role = 2;
    this.USER.createUser(data.value).subscribe((res: any) => {
      console.log(res);
      // limpio formulario
      this.COMPONENT.openSnackBar(res.message, "Aceptar");
      this.AppComponent.loading = false;
    }, err => {
      this.COMPONENT.openSnackBar(err.message, "Aceptar");
    });
  }
}
