import { User } from './../../../interface/user';
import { ComponentsService } from './../../../services/components.service';
import { AppComponent } from './../../../app.component';
import { UserService } from './../../../services/user.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss'],
  providers: [UserService, AppComponent, ComponentsService]

})
export class EditCategoryComponent implements OnInit {

  myform: FormGroup;
  user: User;
  role: any;

  constructor(
    private fb: FormBuilder,
    public USER: UserService,
    public COMPONENT: ComponentsService,
    public AppComponent: AppComponent,
    public dialogRef: MatDialogRef<EditCategoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.role = data.role;
    this.user = data.user;
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  ngOnInit() {
    this.setForm();
    console.log(this.data);
  }
  setForm() {
    this.myform = this.fb.group({
      'name': [this.data.role.name, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(50)])],
      'description': [this.data.role.description, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(500)])],
      
    });
  }

  updateElement(data) {
    data.value.id = this.role.id;
    this.AppComponent.loading = true;
    this.USER.updateElement(this.user.token, data.value, 'categories/updateCategorie').subscribe((res: any) => {
      this.AppComponent.loading = false;
      this.COMPONENT.openSnackBar(res.message, "Aceptar");
      this.dialogRef.close(true);
    }, err => {
      this.COMPONENT.openSnackBar(err.message, "Aceptar");
      this.AppComponent.loading = false;
      this.dialogRef.close();

    })
  }
}

