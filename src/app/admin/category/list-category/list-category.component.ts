import { User } from './../../../interface/user';
import { Router, NavigationStart } from '@angular/router';
import { AppComponent } from './../../../app.component';
import { ComponentsService } from './../../../services/components.service';
import { MatDialog } from '@angular/material';
import { UserService } from './../../../services/user.service';
import { CreateCategoryComponent } from './../create-category/create-category.component';
import { EditCategoryComponent } from './../edit-category/edit-category.component';
import { Component, OnInit, trigger, state, style, animate, transition } from '@angular/core';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss'],
  animations: [
    trigger('shrinkOut', [
      state('in', style({ opacity: 1, transform: 'translateX(0)' })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100%)'
        }),
        animate('0.2s ease-in')
      ]),
      transition('* => void', [
        animate('0.2s 0.1s ease-out', style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ]),
    trigger('scaleEfect', [
      state('in', style({ transform: 'scale(1)' })),
      transition('void => *', [
        style({
          transform: 'scale(0)'
        }),
        animate('0.6s ease-in')
      ]),
      transition('* => void', [
        animate('0.6s ease-out', style({
          transform: 'scale(1)'
        }))
      ])
    ])
  ]
})
export class ListCategoryComponent implements OnInit {

  public category: any
  public user: User;
  constructor(public USER: UserService,
    public dialog: MatDialog,
    public COMPONENT: ComponentsService,
    public AppComponent: AppComponent,
    public router: Router) {
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe((event: NavigationStart) => {
        // You only receive NavigationStart events
        this.user = this.USER.getUser();
      });
  }

  ngOnInit() {
    this.user = this.USER.getUser();
    this.getListElements();
  }


  getListElements() {
    this.USER.getElement(this.user.token,'categories/getCategories').subscribe((res: any) => {
      this.category = res.response;
    }, err => {
      this.COMPONENT.openSnackBar("Se ha presentado un error al consultar la lista de categorias", "Aceptar");
    })
  }

  createElement(): void {
    this.AppComponent.loading = true;
    let dialogRef = this.dialog.open(CreateCategoryComponent, {
      width: '400px',
      data: this.user
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result == true) {
        this.AppComponent.loading = false;
        this.getListElements();
      }
      else {
        this.AppComponent.loading = false;
      }
    });
  }

  deleteElement(item) {

    this.COMPONENT.openConfirmAlert(`Eliminar la categoria ${item.name}?`).then(res => {
      if (res == true) {
        this.AppComponent.loading = true;
        this.USER.deleteElement(this.user.token, item, 'categories/deleteCategorie').subscribe((res: any) => {
          this.COMPONENT.openSnackBar(res.message, "Aceptar");
          this.AppComponent.loading = false;
          this.getListElements();
        }, err => {
          this.AppComponent.loading = false;
          this.COMPONENT.openSnackBar(err.message, "Aceptar");
        })
      }
    })

  }


  openDialogEditElement(item): void {
    this.AppComponent.loading = true;
    let dialogRef = this.dialog.open(EditCategoryComponent, {
      width: '360px',
      data: {
        user: this.user,
        role: item
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result == true) {
        this.AppComponent.loading = false;
        this.getListElements();
      }
      else {
        this.AppComponent.loading = false;
      }
    });
  }

}
