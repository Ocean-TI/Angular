import { User } from './../../../interface/user';
import { ComponentsService } from './../../../services/components.service';
import { AppComponent } from './../../../app.component';
import { UserService } from './../../../services/user.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-edit-role',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.scss'],
  providers: [UserService, AppComponent, ComponentsService]

})
export class EditRoleComponent implements OnInit {

  myform: FormGroup;
  user: User;
  role: any;

  constructor(
    private fb: FormBuilder,
    public USER: UserService,
    public COMPONENT: ComponentsService,
    public AppComponent: AppComponent,
    public dialogRef: MatDialogRef<EditRoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.role = data.role;
    this.user = data.user;
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  ngOnInit() {
    this.setForm();
    console.log(this.data);
  }
  setForm() {
    this.myform = this.fb.group({
      'name': [this.data.role.name, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(50)])],
    });
  }

  updateRole(data) {
    data.value.id = this.role.id;
    this.AppComponent.loading = true;
    this.USER.updateElement(this.user.token, data.value, 'role/updateRole').subscribe((res: any) => {
      this.AppComponent.loading = false;
      this.COMPONENT.openSnackBar(res.message, "Aceptar");
      this.dialogRef.close(true);
    }, err => {
      this.COMPONENT.openSnackBar(err.message, "Aceptar");
      this.AppComponent.loading = false;
      this.dialogRef.close();

    })
  }
}

