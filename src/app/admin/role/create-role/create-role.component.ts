import { ComponentsService } from './../../../services/components.service';
import { AppComponent } from './../../../app.component';
import { UserService } from './../../../services/user.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-create-role',
  templateUrl: './create-role.component.html',
  styleUrls: ['./create-role.component.scss'],
  providers: [UserService, AppComponent, ComponentsService]

})
export class CreateRoleComponent implements OnInit {

  myform: FormGroup;

  constructor(private fb: FormBuilder,
    public USER: UserService,
    public COMPONENT: ComponentsService,
    public AppComponent: AppComponent,
    public dialogRef: MatDialogRef<CreateRoleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.setForm();
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }
  setForm() {
    this.myform = this.fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(7), Validators.maxLength(20)])],
    });
  }

  createRole(data) {
    console.log(data.value);
    this.AppComponent.loading = true;
    this.USER.createElement(this.data.token, data.value, 'role/createRole').subscribe((res: any) => {
      this.AppComponent.loading = false;
      this.COMPONENT.openSnackBar(res.message, "Aceptar");
      this.dialogRef.close(true);
    }, err => {
      this.COMPONENT.openSnackBar(err.message, "Aceptar");
      this.AppComponent.loading = false;
      this.dialogRef.close();
    })
  }

}
