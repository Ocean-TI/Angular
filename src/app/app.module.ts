
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InitComponent } from './init/init.component';
import { RouterModule, Routes } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
// Import HttpClientModule from @angular/common/http
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule, MatMenuModule, MatToolbarModule, MatIconModule, MatCardModule, MatListModule, MatSelectModule, MatInputModule, MatGridListModule, MatTabsModule, MatFormFieldModule, MatSnackBarModule, MatSidenavModule, MatCheckboxModule, MatDialogModule, MatStepperModule, MatTooltipModule, MatProgressBarModule
} from '@angular/material';


// App components
import { AppComponent } from './app.component';
import { LogoutComponent } from './login/logout/logout.component';
import { MenuComponent } from './menu/menu.component';
import { ProfileComponent } from './profile/profile.component';
import { EditProfileComponent } from './profile/edit-profile/edit-profile.component';
import { EditPasswordComponent } from './profile/edit-password/edit-password.component';
import { HomeComponent } from './home/home.component';
import { HomeClientComponent } from './home/home-client/home-client.component';
import { HomeAdminComponent } from './home/home-admin/home-admin.component';
import { EditBusinessComponent } from './home/home-client/edit-business/edit-business.component';
import { CreateBusinessComponent } from './home/home-client/create-business/create-business.component';
import { RegistryComponent } from './registry/registry.component';
import { LoginComponent } from './login/login.component';
import { TestFormComponent } from './test-form/test-form.component';
import { CreateRoleComponent } from './admin/role/create-role/create-role.component';
import { EditRoleComponent } from './admin/role/edit-role/edit-role.component';
import { ListRoleComponent } from './admin/role/list-role/list-role.component';
import { ConfirmAlertComponent } from './services/confirm-alert/confirm-alert.component';
import { RolHomeComponent } from './admin/role/rol-home/rol-home.component';
import { CategoryComponent } from './admin/category/category.component';
import { CreateCategoryComponent } from './admin/category/create-category/create-category.component';
import { EditCategoryComponent } from './admin/category/edit-category/edit-category.component';
import { ListCategoryComponent } from './admin/category/list-category/list-category.component';
import { ContentLoginComponent } from './login/content-login/content-login.component';

// Config routes
const appRoutes: Routes = [
  { path: 'login', component: ContentLoginComponent },
  { path: 'registry', component: RegistryComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'init', component: InitComponent },
  { path: 'role', component: RolHomeComponent },
  { path: 'category', component: CategoryComponent },
  { path: 'home', component: HomeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    InitComponent,
    LoginComponent,
    RegistryComponent,
    LogoutComponent,
    MenuComponent,
    ProfileComponent,
    EditProfileComponent,
    EditPasswordComponent,
    HomeComponent,
    CreateBusinessComponent,
    EditBusinessComponent,
    HomeClientComponent,
    HomeAdminComponent,
    TestFormComponent,
    CreateRoleComponent,
    EditRoleComponent,
    ListRoleComponent,
    ConfirmAlertComponent,
    RolHomeComponent,
    CategoryComponent,
    CreateCategoryComponent,
    EditCategoryComponent,
    ListCategoryComponent,
    ContentLoginComponent
  ],
  entryComponents: [
    CreateBusinessComponent,
    EditBusinessComponent,
    CreateRoleComponent,
    EditRoleComponent,
    ConfirmAlertComponent,
    EditCategoryComponent,
    CreateCategoryComponent
  ],
  imports: [
    BrowserAnimationsModule,

    // material
    MatButtonModule,
    MatMenuModule,
    MatTabsModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatListModule,
    MatStepperModule,
    MatInputModule,
    MatGridListModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatSelectModule,
    MatTooltipModule,
    // Material
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    FlexLayoutModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
