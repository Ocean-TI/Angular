import { ComponentsService } from './../../services/components.service';
import { AppComponent } from './../../app.component';
import { Router, NavigationStart } from '@angular/router';
import { UserService } from './../../services/user.service';
import { User } from './../../interface/user';
import { Component, OnInit, trigger, state, style, animate, transition } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss'],
  providers: [UserService],
  animations: [
    trigger('shrinkOut', [
      state('in', style({ opacity: 1, transform: 'translateX(0)' })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100%)'
        }),
        animate('0.2s ease-in')
      ]),
      transition('* => void', [
        animate('0.2s 0.1s ease-out', style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ]),
    trigger('scaleEfect', [
      state('in', style({ transform: 'scale(1)' })),
      transition('void => *', [
        style({
          transform: 'scale(0)'
        }),
        animate('0.6s ease-in')
      ]),
      transition('* => void', [
        animate('0.6s ease-out', style({
          transform: 'scale(1)'
        }))
      ])
    ])
  ]
})
export class EditProfileComponent implements OnInit {
  myform: FormGroup;
  public user: User;
  public editUser: Boolean = false;
  constructor(private fb: FormBuilder,
    public router: Router,
    public USER: UserService,
    public COMPONENT: ComponentsService,
    public AppComponent: AppComponent
  ) {
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe((event: NavigationStart) => {
        // You only receive NavigationStart events
        this.user = this.USER.getUser();
      });
  }

  ngOnInit() {
    this.user = this.USER.getUser();
    this.setForm();
  }

  setForm() {
    this.myform = this.fb.group({
      'name_user': [this.user.name_user, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30)])],
      'name': [this.user.name, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30)])],
      'last_name': [this.user.last_name, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(30)])],
      'phone': [this.user.phone, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(20)])],
      'gender': [this.user.genderid, Validators.required],
    });
  }

  editProfileUser() {
    this.editUser = !this.editUser;
  }

  editProfile(data) {
    this.AppComponent.loading = true;
    this.USER.updateProfile(this.user.token, data.value).subscribe((res: any) => {
      this.AppComponent.loading = false;
      res.response.login = true;
      this.editUser = false;
      let user = JSON.stringify(res.response);
      this.USER.setUser(user);
      this.user = this.USER.getUser();
      this.COMPONENT.openSnackBar("Perfil actualizado", "Aceptar");
      this.setForm();

    }, err => {
      this.AppComponent.loading = false;
    })
  }

}
