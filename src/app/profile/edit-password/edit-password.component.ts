import { ComponentsService } from './../../services/components.service';
import { AppComponent } from './../../app.component';
import { Router, NavigationStart } from '@angular/router';
import { UserService } from './../../services/user.service';
import { User } from './../../interface/user';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-password',
  templateUrl: './edit-password.component.html',
  styleUrls: ['./edit-password.component.scss']
})
export class EditPasswordComponent implements OnInit {
  myform: FormGroup;
  public user: User;
  constructor(private fb: FormBuilder,
    public router: Router,
    public AppComponent: AppComponent,
    public COMPONENT: ComponentsService,
    public USER: UserService
  ) {
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe((event: NavigationStart) => {
        // You only receive NavigationStart events
        this.user = this.USER.getUser();
      });
  }

  ngOnInit() {
    this.user = this.USER.getUser();
    this.setForm();
  }
  setForm() {
    this.myform = this.fb.group({
      'pwd': [null, Validators.compose([Validators.required, Validators.minLength(7), Validators.maxLength(20)])],
      'confirmpwd': [null, Validators.compose([Validators.required, Validators.minLength(7), Validators.maxLength(20)])],
    });
  }


  changePassword(data) {

    console.log(data.value);

    if (data.value.pwd == data.value.confirmpwd) {
      this.AppComponent.loading = true;
      this.USER.updatePassword(this.user.token, data.value).subscribe((res: any) => {
        this.AppComponent.loading = false;
        this.COMPONENT.openSnackBar(res.message, "Aceptar");
        this.setForm();
      }, err => {
        this.AppComponent.loading = false;
      })
    }
    else {
      this.COMPONENT.openSnackBar("Las contraseñas no son iguales", "Aceptar");

    }
  }

}
