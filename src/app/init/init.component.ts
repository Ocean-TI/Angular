import { UserService } from '../services/user.service';
import { User } from '../interface/user';
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styleUrls: ['./init.component.css']
})
export class InitComponent implements OnInit {
  public user: User;

  constructor(public router: Router,
    public USER: UserService) {
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe((event: NavigationStart) => {
        // You only receive NavigationStart events
        this.user = this.USER.getUser();
      });
  }

  ngOnInit() {
    this.user = this.USER.getUser();
  }

}
