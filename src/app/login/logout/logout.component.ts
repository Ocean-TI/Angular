import { AppComponent } from './../../app.component';
import { Router, NavigationStart } from '@angular/router';
import { ComponentsService } from './../../services/components.service';
import { User } from './../../interface/user';
import { UserService } from './../../services/user.service';
import { Component, OnInit, Input } from '@angular/core';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
  providers: [UserService, ComponentsService]
})
export class LogoutComponent implements OnInit {
  public user: any;
  @Input() action: string;

  constructor(public USER: UserService,
    public router: Router,
    public AppComponent: AppComponent,
    public COMPONENT: ComponentsService) {
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe((event: NavigationStart) => {
        this.user = <User>this.USER.getUser();
      });
  }

  ngOnInit() {
    this.user = <User>this.USER.getUser();
  }

  logout() {
    this.AppComponent.loading = true;
    return new Promise(resolve => {
      this.USER.logout(this.user.token).subscribe((res: any) => {
        this.COMPONENT.openSnackBar(res.message, "Aceptar");
        this.user = this.USER.getEmptyUser()
        this.USER.setUser(JSON.stringify(this.user));
        this.AppComponent.loading = false;
        // oculto el menú
        this.AppComponent.stateSideNav = false;
        this.router.navigate(['/login']);
        resolve(true);
      }, err => {
        this.AppComponent.stateSideNav = false;
        this.AppComponent.loading = false;
        this.router.navigate(['/login']);
        this.COMPONENT.openSnackBar("Se ha presentado un fallo al cerrar la sesión", "Aceptar");

      })
    })
  }
}
