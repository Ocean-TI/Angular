import { AppComponent } from './../app.component';
import { ComponentsService } from './../services/components.service';
import { UserService } from './../services/user.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [UserService, ComponentsService]

})
export class LoginComponent implements OnInit {

  myform: FormGroup;

  constructor(private USER: UserService,
    private fb: FormBuilder,
    public router: Router,
    public AppComponent: AppComponent,
    public COMPONENT: ComponentsService) {
  }
  ngOnInit() {
    this.myform = this.fb.group({
      'name_user': [null, Validators.required],
      'pwd': [null, Validators.compose([Validators.required, Validators.minLength(7), Validators.maxLength(20)])],
    });
  }

  loginUser(data) {
    this.AppComponent.loading = true;
    this.USER.loginUser(data.value).subscribe((res: any) => {
      setTimeout(() => {
        this.USER.getProfileUser(res.token).subscribe((res: any) => {

          // Validación de permisos
          if (res.response.role == 1 || res.response.role == 2) {
            // limpio formulario
            this.myform.reset();
            this.COMPONENT.openSnackBar("has iniciado sesión con " + res.response.email, "Aceptar");
            res.response.login = true;
            let user = JSON.stringify(res.response);
            this.USER.setUser(user);
            this.AppComponent.loading = false;
            this.AppComponent.stateSideNav = true;
            this.router.navigate(['/home']);
          } else {
            this.AppComponent.loading = false;
            this.COMPONENT.openSnackBar("Tu usuario no posee permiso para ingresar a este módulo", "Aceptar");
          }
        })
      }, 300)
    }, err => {
      this.AppComponent.loading = false;
      this.COMPONENT.openSnackBar(err.error.message, "Aceptar");
    });
  }

  // openSnackBar(message: string, action: string) {
  //   this.snackBar.open(message, action, {
  //     duration: 2000,
  //   });
  // }

}
