export interface User {
    login: boolean,
    name_user: string,
    name: string,
    role: number,
    last_name: string,
    pwd: string,
    email: string,
    token: string,
    phone: string,
    gender: string,
    genderid: number,
    created_date: string
}
