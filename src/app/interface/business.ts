export interface Business {
    name: string,
    address: string,
    phone: string,
    idbusiness: number,
    created_date: string,
    fullname_contact: string
}
