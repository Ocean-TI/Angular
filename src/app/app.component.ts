import { User } from './interface/user';
import { UserService } from './services/user.service';
import { ComponentsService } from './services/components.service';
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';
import { Component, ViewChild, ViewEncapsulation, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ComponentsService, UserService],
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
})

export class AppComponent implements OnInit {
  // Sidemenu
  @ViewChild('sidenav') sidenav: MatSidenav;
  public user: User;
  public loading: boolean = false;
  public stateSideNav = true;
  public nameApp: string = "Demter";

  constructor(private router: Router,
    public USER: UserService,
    public COMPONENT: ComponentsService) {

    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe((event: NavigationStart) => {
        // You only receive NavigationStart events
        this.getDataUser();
      });
  }
  ngOnInit() {
    // funcion para validar la sesión del usuario
    this.getDataUser();
    this.valdiateRouterUser();
  }

  getDataUser() {
    this.user = this.USER.getUser();
  }

  valdiateRouterUser() {
    if (this.user.login == true) {
      this.USER.validateToken(this.user.token).subscribe(res => {
        this.COMPONENT.openSnackBar("has iniciado sesión con " + this.user.email, "Aceptar");
        this.router.navigate(['/home']).then(res => {
          this.stateSideNav = true;
        });
      }, err => {
        this.COMPONENT.openSnackBar("Se ha cerrado la sesión, Inicia de nuevo", "Aceptar");
        this.user = this.USER.getEmptyUser();
        this.USER.setUser(JSON.stringify(this.user));
        this.stateSideNav = false;
        this.router.navigate(['/login']);
      })
    }
    else {
      this.user = this.USER.getEmptyUser();
      this.USER.setUser(JSON.stringify(this.user));
      this.stateSideNav = false;
      this.router.navigate(['/login']);
    }
  }


  toggleMenu() {
    this.sidenav.toggle();
  }
}
