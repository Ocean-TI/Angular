import { TestBed, inject } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material';
import { ComponentsService } from './components.service';

describe('ComponentsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ComponentsService, MatSnackBar]
    });
  });

  it('should be created', inject([ComponentsService], (service: ComponentsService) => {
    expect(service).toBeTruthy();
  }));
});
