import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-confirm-alert',
  templateUrl: './confirm-alert.component.html',
  styleUrls: ['./confirm-alert.component.scss']
})
export class ConfirmAlertComponent implements OnInit {
  info: any;
  constructor(public dialogRef: MatDialogRef<ConfirmAlertComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.info = data;
  }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close(false);
  }

  confirmClick(): void {
    this.dialogRef.close(true);
  }
}
