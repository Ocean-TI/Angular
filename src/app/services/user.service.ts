import { User } from './../interface/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const emptyuser: any = {
  login: false,
  pwd: '',
  email: '',
  token: '',
  name: '',
  last_name: '',
  name_user: '',
  phone: '',
  created_date: '',
  genderid: 0,
  gender: '',
  role: 0
};

@Injectable()
export class UserService {

  url: string = 'http://appservicios.ingeneo.co/api/';

  constructor(private http: HttpClient) { }

  getEmptyUser() {
    return emptyuser;
  }
  getUser() {
    let user: User = JSON.parse(localStorage.getItem("user"));
    return user || emptyuser;
  }
  setUser(data) {
    localStorage.setItem("user", data);
    let user: User = JSON.parse(localStorage.getItem("user"));
    return user || emptyuser;
  }

  createUser(data) {
    return this.http.post(`${this.url}registry/createUser`, data);
  }
  loginUser(data) {

    return this.http.post(`${this.url}auth/loginUser`, data);
  }

  validateToken(token) {

    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.get(`${this.url}auth/validateToken`, headers);
  }

  logout(token) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.get(`${this.url}auth/logout`, headers);
  }

  updateProfile(token, data) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.post(`${this.url}profile/updateProfile`, data, headers);
  }

  updatePassword(token, data) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.post(`${this.url}auth/updatePassword`, data, headers);
  }

  getProfileUser(token) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.get(`${this.url}profile/profile`, headers);
  }



  // Business
  createBusiness(token, data) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.post(`${this.url}business/createBusiness`, data, headers);
  }
  getBusiness(token, data) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.post(`${this.url}business/getBusiness`, data, headers);
  }
  changeStateBusiness(token, data) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.post(`${this.url}business/inactiveBusiness`, data, headers);
  }

  deleteBusiness(token, data) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.post(`${this.url}business/deleteBusiness`, data, headers);
  }

  updateBusiness(token, data) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.post(`${this.url}business/updateBusiness`, data, headers);
  }



  // Admin


  // General Services
  getElement(token, path) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.get(`${this.url + path}`, headers);
  }
  createElement(token, data, path) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.post(`${this.url + path}`, data, headers);
  }
  updateElement(token, data, path) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.post(`${this.url + path}`, data, headers);
  }

  deleteElement(token, data, path) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      })
    };
    return this.http.post(`${this.url + path}`, data, headers);
  }

}
