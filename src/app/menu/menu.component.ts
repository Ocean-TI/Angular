import { User } from './../interface/user';
import { LogoutComponent } from './../login/logout/logout.component';
import { AppComponent } from './../app.component';
import { UserService } from './../services/user.service';
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';
import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import 'rxjs/add/operator/filter';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  providers: [UserService, LogoutComponent]
})
export class MenuComponent implements OnInit {
  @Output() navToggle = new EventEmitter<boolean>();

  public user: User;
  
  constructor(public router: Router,
    public AppComponent: AppComponent,
    public LogoutComponent: LogoutComponent,
    public USER: UserService) {
    this.router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe((event: NavigationStart) => {
        // You only receive NavigationStart events
        this.user = this.USER.getUser();
      });
  }

  ngOnInit() {
    this.user = this.USER.getUser();
  }
  // navOpen() {
  //   this.navToggle.emit(true);
  // }
  toggleMenu() {
    this.AppComponent.sidenav.toggle();
  }

  logout() {
    this.LogoutComponent.logout();
  }



}
